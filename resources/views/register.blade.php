<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ route('welcome') }}" method="POST">
        @csrf
        <label for="firstname">First name:</label><br>
        <input type="text" name="firstname" required><br><br>
        <label for="lastname">Last name:</label><br>
        <input type="text" name="lastname" required><br><br>
        <label for="gander">Gander:</label><br>
        <input type="radio" name="gander" value="male" required>Male <br>
        <input type="radio" name="gander" value="female">Female <br>
        <input type="radio" name="gander" value="other">Other <br><br>
        <label for="nationality">Nationality</label><br>
        <select name="nationality" id="">
            <option value="indonesia">Indonesia</option><br>
            <option value="amerika">Amerika</option><br>
            <option value="inggris">Inggris</option><br>
        </select><br><br>
        <label for="languagespoken">Language Spoken</label><br>
        <input type="checkbox" name="languagespoken" value="indonesia" required>Indonesia <br>
        <input type="checkbox" name="languagespoken" value="english">English <br>
        <input type="checkbox" name="languagespoken" value="other">Other <br><br>
        <label for="bio">Bio</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10" required></textarea><br><br>
        <button type="submit">Sign Up</button>
    </form>
</body>

</html>
